void fnTask_TEMPERATURE(int iID)
{
  temperature = thermister(analogRead(5));
} 

/*==================================================================================*/

double thermister(int RawADC) 
{
  double Temp = 0;
  Temp = log(((10240000/RawADC) - 10000));
  Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
  Temp = Temp - 257.15; 
  return Temp;              
}
