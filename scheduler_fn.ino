void fnScheduler()
{
  unsigned long lNow = millis();
  unsigned long lTmp;

  for(int i=0; i<TASKS_NUM; i++) {
    if(stTasks[i].bState == STATE_RUNNING){
      if(stTasks[i].lNextRun <= lNow) {
        stTasks[i].lLastRun = lNow; 
        lTmp = micros();
        stTasks[i].fnCallBack(stTasks[i].iTaskID);
        stTasks[i].lDuration = micros()-lTmp;
        if(stTasks[i].bType == TYPE_CYCLIC){  
          stTasks[i].lNextRun += stTasks[i].lPeriod;
        }
        else{
          stTasks[i].bState = STATE_SLEEPING;
        }
      }
    }  
  }
}

/*==================================================================================*/


