void fnTask_TLACITKA(int iID)
{
  tlacitka();

  if(state_dis==STATE_WORKOUT){
    kola();
  }
}

/*==================================================================================*/

/**************************************************************************/
//    (digitalRead(14) == LOW)      TLACITKO NAHORU   (ZMACKNUTE)
//    (digitalRead(15) == LOW)      TLACITKO DOLEVA   (ZMACKNUTE)
//    (digitalRead(16) == LOW)      TLACITKO ENTER    (ZMACKNUTE)
//    (digitalRead(17) == LOW)      TLACITKO DOLU     (ZMACKNUTE)
//    (digitalRead(18) == LOW)      TLACITKO DOPRAVA  (ZMACKNUTE)
/**************************************************************************/

void tlacitka()
{

  // Reset
  if ((digitalRead(15) == LOW)&&(digitalRead(16) == LOW)&&(digitalRead(18) == LOW)){   
    lcd.clear();
    state_dis = STATE_INIC1;
    state_makej = STATE_PRVNI;
    stTasks[TASK_VYHODNOCENI].bState = STATE_SLEEPING;
    stTasks[TASK_TEPLOTA].bState = STATE_SLEEPING;
    stTasks[TASK_INIC].bState = STATE_RUNNING;
  } 
}

void kola()
{

  if (digitalRead(16) == LOW){
    ukaz(poradi, kolo, borci);
    if(poradi < borci){
      poradi++;
    }
    else if(kolo < opak){
      kolo++;
      poradi = 1;  
    }
  }
}

void ukaz(int borec_na_rade, int cislo_kola, int celkovy_pocet_borcu)
{
  switch(celkovy_pocet_borcu){
    
    case 1:
      lcd.setCursor(0, 3);
      lcd.print(' ');
      lcd.setCursor(0, 3);
      lcd.write(byte(2));    
      break;  
    
    
    case 2:
      if(borec_na_rade == 1){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.write(byte(2)); 
      }
      else{
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 3);
        lcd.write(byte(2));
      }
      break;
    
    
    case 3:
      if(borec_na_rade == 1){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.write(byte(2)); 
      }
      else if(borec_na_rade == 2){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.write(byte(2)); 
      }
      else{
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 3);
        lcd.write(byte(2)); 
      }
      break;
    
    
    case 4:
      if(borec_na_rade == 1){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 0);
        lcd.print(' ');
        lcd.setCursor(0, 0);
        lcd.write(byte(2)); 
      }
      else if(borec_na_rade == 2){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 0);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.write(byte(2)); 
      }
      else if(borec_na_rade == 3){
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 0);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.write(byte(2)); 
      }
      else{
        lcd.setCursor(0, 3);
        lcd.print(' ');
        lcd.setCursor(0, 2);
        lcd.print(' ');
        lcd.setCursor(0, 1);
        lcd.print(' ');
        lcd.setCursor(0, 0);
        lcd.print(' ');
        lcd.setCursor(0, 3);
        lcd.write(byte(2));
      }
      break;


      default:
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("ERROR TLAC");                                 
        break;
  }

  
}
