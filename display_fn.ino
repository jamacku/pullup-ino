void fnTask_ZOBRAZENI(int iID)
{
  if(state_dis!=STATE_WORKOUT){
    zobraz_inic();
  }
  else if((state_dis==STATE_WORKOUT) && (kolo < opak) && (poradi < borci)){
    zobraz_hodnoty();
  }
  else if((state_dis==STATE_WORKOUT) && (kolo == opak) && (poradi == borci)){
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Konec");
  }
}

/*==================================================================================*/

void getCharNum(long lNum, char *cNum)
{
  if(lNum < 10){
    cNum[0] = ' ';
    cNum[1] = lNum + int('0'); 
  }
  else{
    cNum[0] = lNum / 10 + int('0');
    cNum[1] = lNum % 10 + int('0');  
  }
  if(lNum > 99){
    cNum[0] = int('*');
    cNum[1] = int('*');
  }
  cNum[2] = '\0';
}

/////////////////////////////////////////////////////////////////////////////////////////

void zobraz_inic()
{
  if(state_dis==STATE_START) {
    
  }
  else if((state_dis==STATE_INIC1) || (state_dis==STATE_INIC2)) {
    if(state_dis==STATE_INIC1){
      lcd.setCursor(0, 0);
      lcd.print(' ');
      lcd.setCursor(0, 0);
      lcd.write(byte(2));
      lcd.setCursor(0, 1);
      lcd.print(' ');
      lcd.setCursor(0, 1);
      lcd.write(byte(1));
    }
    else if(state_dis==STATE_INIC2){
      lcd.setCursor(0, 0);
      lcd.print(' ');
      lcd.setCursor(0, 0);
      lcd.write(byte(0));
      lcd.setCursor(0, 1);
      lcd.print(' ');
      lcd.setCursor(0, 1);
      lcd.write(byte(2));
    }
    lcd.setCursor(13, 0);
    getCharNum((long)borci, Sborci);
    lcd.print(Sborci);
    lcd.setCursor(17, 1);
    getCharNum((long)opak, Sopak);
    lcd.print(Sopak);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////

void zobraz_hodnoty()
{
  lcd.setCursor(16, 0);
  getCharNum((long)temperature, sT);
  lcd.print(sT);
  
  if (borci == 1){ 
    lcd.setCursor(5, 3);
    getCharNum((long)stOsoba[TASK_PRVNI].pocet[0], stOsoba[TASK_PRVNI].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP);     
    lcd.setCursor(12, 3);
    getCharNum((long)stOsoba[TASK_PRVNI].total, stOsoba[TASK_PRVNI].sTot);
    lcd.print(stOsoba[TASK_PRVNI].sTot);
  
  }
    
  if (borci == 2){
    lcd.setCursor(5, 2);
    getCharNum((long)stOsoba[TASK_PRVNI].pocet[0], stOsoba[TASK_PRVNI].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP); 
    lcd.setCursor(12, 2);
    getCharNum((long)stOsoba[TASK_PRVNI].total, stOsoba[TASK_PRVNI].sTot);
    lcd.print(stOsoba[TASK_PRVNI].sTot);
    
    lcd.setCursor(5, 3);
    getCharNum((long)stOsoba[TASK_DRUHY].pocet[0], stOsoba[TASK_DRUHY].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP);
    lcd.setCursor(12, 3);
    getCharNum((long)stOsoba[TASK_DRUHY].total, stOsoba[TASK_DRUHY].sTot);
    lcd.print(stOsoba[TASK_DRUHY].sTot);
    
  }
    
  if (borci == 3){   
    lcd.setCursor(5, 1);
    getCharNum((long)stOsoba[TASK_PRVNI].pocet[0], stOsoba[TASK_PRVNI].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP); 
    lcd.setCursor(12, 1);
    getCharNum((long)stOsoba[TASK_PRVNI].total, stOsoba[TASK_PRVNI].sTot);
    lcd.print(stOsoba[TASK_PRVNI].sTot);
     
    lcd.setCursor(5, 2);
    getCharNum((long)stOsoba[TASK_DRUHY].pocet[0], stOsoba[TASK_DRUHY].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP);
    lcd.setCursor(12, 2);
    getCharNum((long)stOsoba[TASK_DRUHY].total, stOsoba[TASK_DRUHY].sTot);
    lcd.print(stOsoba[TASK_DRUHY].sTot);
    
    lcd.setCursor(5, 3);
    getCharNum((long)stOsoba[TASK_TRETI].pocet[0], stOsoba[TASK_TRETI].sP);
    lcd.print(stOsoba[TASK_TRETI].sP);
    lcd.setCursor(12, 3);
    getCharNum((long)stOsoba[TASK_TRETI].total, stOsoba[TASK_TRETI].sTot);
    lcd.print(stOsoba[TASK_TRETI].sTot);
   
  }
    
  if (borci == 4){    
    lcd.setCursor(5, 0);
    getCharNum((long)stOsoba[TASK_PRVNI].pocet[0], stOsoba[TASK_PRVNI].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP); 
    lcd.setCursor(12, 0);
    getCharNum((long)stOsoba[TASK_PRVNI].total, stOsoba[TASK_PRVNI].sTot);
    lcd.print(stOsoba[TASK_PRVNI].sTot);
      
    lcd.setCursor(5, 1);
    getCharNum((long)stOsoba[TASK_DRUHY].pocet[0], stOsoba[TASK_DRUHY].sP);
    lcd.print(stOsoba[TASK_PRVNI].sP);
    lcd.setCursor(12, 1);
    getCharNum((long)stOsoba[TASK_DRUHY].total, stOsoba[TASK_DRUHY].sTot);
    lcd.print(stOsoba[TASK_DRUHY].sTot);
      
    lcd.setCursor(5, 2);
    getCharNum((long)stOsoba[TASK_TRETI].pocet[0], stOsoba[TASK_TRETI].sP);
    lcd.print(stOsoba[TASK_TRETI].sP);
    lcd.setCursor(12, 2);
    getCharNum((long)stOsoba[TASK_TRETI].total, stOsoba[TASK_TRETI].sTot);
    lcd.print(stOsoba[TASK_TRETI].sTot);
     
    lcd.setCursor(5, 3);
    getCharNum((long)stOsoba[TASK_CTVRTY].pocet[0], stOsoba[TASK_CTVRTY].sP);
    lcd.print(stOsoba[TASK_CTVRTY].sP);
    lcd.setCursor(12, 3);
    getCharNum((long)stOsoba[TASK_CTVRTY].total, stOsoba[TASK_CTVRTY].sTot);
    lcd.print(stOsoba[TASK_CTVRTY].sTot);   
  }
}
